export function errorMessageNoProperty(keyProperty: string): Error {
  return new Error(`No property -->${keyProperty}<--`);
}
