import { IRotateble } from '../../BaseObjects/Interfaces/IRotateble';
import { ICommand } from '../../BaseObjects/Interfaces/ICommand';

export class Rotate implements ICommand {
  rotateble: IRotateble;

  constructor(rotateble: IRotateble) {
    this.rotateble = rotateble;
  }

  execute(): void {
    // ref - придумать миханизм поворота
    const newPosition =
      this.rotateble.getRotatePosition() + this.rotateble.getRotateVelosity();
    this.rotateble.setRotatePosition(newPosition);
  }
}
