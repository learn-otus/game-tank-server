import { ICommand } from '../../BaseObjects/Interfaces/ICommand';
import { IMovable } from '../../BaseObjects/Interfaces/IMovable';

export class Move implements ICommand {
  movable: IMovable;

  constructor(movable: IMovable) {
    this.movable = movable;
  }

  public execute(): void {
    this.movable.setPosition(
      this.movable.getPosition().add(this.movable.getVelosity())
    );
  }
}
