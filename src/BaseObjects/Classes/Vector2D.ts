import { IVector2D } from '../Interfaces/IVector2D';

export class Vector2D implements IVector2D {
  x: number;
  y: number;

  constructor(startPosition: IVector2D) {
    this.x = startPosition.x;
    this.y = startPosition.y;
  }

  public add(vector: IVector2D): Vector2D {
    this.x = this.x + vector.x;
    this.y = this.y + vector.y;
    return this;
  }
}
