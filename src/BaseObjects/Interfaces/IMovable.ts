import { Vector2D } from '../Classes/Vector2D';

export interface IMovable {
  getPosition(): Vector2D;
  setPosition: (vector: Vector2D) => void;
  getVelosity(): Vector2D;
}
