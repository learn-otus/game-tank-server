export interface IRotateble {
  getRotatePosition(): number;
  setRotatePosition: (velosity: number) => void;
  getRotateVelosity(): number;
}
