/* eslint-disable  @typescript-eslint/no-explicit-any */
export interface UObj {
  getProperty: (key: string) => any;
  setProperty: (key: string, value: any) => void;
}
