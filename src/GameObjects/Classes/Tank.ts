/* eslint-disable  @typescript-eslint/no-explicit-any */
import { UObj } from '../../BaseObjects/Interfaces/UObj';
import { Vector2D } from '../../BaseObjects/Classes/Vector2D';

export class Tank implements UObj {
  private attributes: any;

  constructor(config: unknown) {
    this.attributes = config;
  }

  getProperty(key: string): Vector2D {
    return this.attributes[key];
  }

  setProperty(key: string, value: Vector2D): void {
    this.attributes[key] = value;
  }
}
