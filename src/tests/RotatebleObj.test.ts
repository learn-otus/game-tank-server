import { Tank } from '../GameObjects/Classes/Tank';
import { Rotate } from '../Commands/Classes/Rotate';
import { RotatebleAdapter } from '../Adapters/RotatebleAdapter';
import {
  ROTATE_POSITION_KEY,
  ROTATE_VELOSITY_KEY
} from '../Adapters/RotatebleAdapter';

describe('Реализация Rotate на различных объектах', () => {
  describe('Проверка объекта Tank на умение поворачивать', () => {
    const rotatePosition = 160;
    const rotateVelosity = 10;
    const expectedValue = rotatePosition + rotateVelosity;

    const myRotatebleTank = new Tank({
      [ROTATE_POSITION_KEY]: rotatePosition,
      [ROTATE_VELOSITY_KEY]: rotateVelosity
    });

    const rotateTank = new Rotate(new RotatebleAdapter(myRotatebleTank));
    // prettier-ignore
    test(`Изменение позиции поворота-> ${JSON.stringify(myRotatebleTank)}`, () => {
      rotateTank.execute();
      expect(myRotatebleTank.getProperty(ROTATE_POSITION_KEY)).toBe(
        expectedValue
      );
    });
  });

  describe('Проверка Танка на отсутствие нужных св-тв для поворота', () => {
    const tankNoRotatePosition = new Tank({
      [ROTATE_VELOSITY_KEY]: 10
    });
    const tankNoRotateVelosity = new Tank({
      [ROTATE_POSITION_KEY]: 100
    });
    const rotateTankNoRotatePosition = new Rotate(
      new RotatebleAdapter(tankNoRotatePosition)
    );
    const rotateTankNoRotateVelosity = new Rotate(
      new RotatebleAdapter(tankNoRotateVelosity)
    );

    test(`Исключение(Ошибка) если нет св-ва ${ROTATE_POSITION_KEY}`, () => {
      expect(() => rotateTankNoRotatePosition.execute()).toThrow();
    });
    test(`Исключение(Ошибка) если нет св-ва ${ROTATE_VELOSITY_KEY}`, () => {
      expect(() => rotateTankNoRotateVelosity.execute()).toThrow();
    });
  });
});
