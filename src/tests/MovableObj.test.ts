import { Vector2D } from '../BaseObjects/Classes/Vector2D';
import { Tank } from '../GameObjects/Classes/Tank';
import { Move } from '../Commands/Classes/Move';
import {
  MovableAdapter,
  POSITION_KEY,
  VELOSITY_KEY
} from '../Adapters/MovableAdapter';

describe('Реализация Move на различных объектах', () => {
  describe('Проверка объекта Tank на движение', () => {
    const position = { x: 12, y: 5 };
    const velosity = { x: -7, y: 3 };
    const expectedValue = {
      x: position.x + velosity.x,
      y: position.y + velosity.y
    };

    const myTank = new Tank({
      [POSITION_KEY]: new Vector2D(position),
      [VELOSITY_KEY]: new Vector2D(velosity)
    });

    const moveMyTank = new Move(new MovableAdapter(myTank));
    // prettier-ignore
    test(`Изменение позиции-> position = ${JSON.stringify(position)}, velosity = ${JSON.stringify(velosity)} => newPosition = ${JSON.stringify(expectedValue)}`, () => {
      moveMyTank.execute();
      expect(myTank.getProperty('position')).toEqual(
        new Vector2D(expectedValue)
      );
    });
  });
  describe('Проверка объекта Tank который не наделен св-ми движения', () => {
    const tankNoPosition = new Tank({
      [VELOSITY_KEY]: new Vector2D({ x: 1, y: 11 })
    });

    const tankNoVelosity = new Tank({
      [POSITION_KEY]: new Vector2D({ x: 10, y: 11 })
    });

    const tankMovaNoPos = new Move(new MovableAdapter(tankNoPosition));
    const tankMovaNoVel = new Move(new MovableAdapter(tankNoVelosity));

    // prettier-ignore
    test(`Танк без скорости ${JSON.stringify(tankNoVelosity)} - приводит к ошибке`, () => {
      expect(() => tankMovaNoVel.execute()).toThrowError()
    })

    // prettier-ignore
    test(`Танк без позиции ${JSON.stringify(tankNoPosition)} - приводит к ошибке`, () => {
      expect(() => tankMovaNoPos.execute()).toThrowError()
    })
  });
});
