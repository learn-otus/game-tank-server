import { IMovable } from '../BaseObjects/Interfaces/IMovable';
import { Vector2D } from '../BaseObjects/Classes/Vector2D';
import { errorMessageNoProperty } from '../Functions/helpers';
import { UObj } from '../BaseObjects/Interfaces/UObj';

export const POSITION_KEY = 'position';
export const VELOSITY_KEY = 'velosity';

export class MovableAdapter implements IMovable {
  gameObj: UObj;

  constructor(gameObj: UObj) {
    this.gameObj = gameObj;
  }

  getPosition(): Vector2D {
    if (!this.gameObj.getProperty(POSITION_KEY)) {
      throw errorMessageNoProperty(POSITION_KEY);
    }
    return this.gameObj.getProperty(POSITION_KEY);
  }

  setPosition(vector: Vector2D): void {
    if (!this.gameObj.getProperty(POSITION_KEY)) {
      throw errorMessageNoProperty(POSITION_KEY);
    }
    this.gameObj.setProperty(POSITION_KEY, vector);
  }

  getVelosity(): Vector2D {
    if (!this.gameObj.getProperty(VELOSITY_KEY)) {
      throw errorMessageNoProperty(VELOSITY_KEY);
    }
    return this.gameObj.getProperty(VELOSITY_KEY);
  }
}
