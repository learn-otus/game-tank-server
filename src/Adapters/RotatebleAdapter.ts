import { IRotateble } from '../BaseObjects/Interfaces/IRotateble';
import { UObj } from '../BaseObjects/Interfaces/UObj';
import { errorMessageNoProperty } from '../Functions/helpers';

export const ROTATE_POSITION_KEY = 'rotate-position';
export const ROTATE_VELOSITY_KEY = 'rotate-velosity';

export class RotatebleAdapter implements IRotateble {
  private gameObj: UObj;

  constructor(gameObj: UObj) {
    this.gameObj = gameObj;
  }

  getRotatePosition(): number {
    if (!this.gameObj.getProperty(ROTATE_POSITION_KEY)) {
      throw errorMessageNoProperty(ROTATE_POSITION_KEY);
    }
    return this.gameObj.getProperty(ROTATE_POSITION_KEY);
  }

  setRotatePosition(velosity: number): void {
    if (!this.gameObj.getProperty(ROTATE_POSITION_KEY)) {
      throw errorMessageNoProperty(ROTATE_POSITION_KEY);
    }
    this.gameObj.setProperty(ROTATE_POSITION_KEY, velosity);
  }
  getRotateVelosity(): number {
    if (!this.gameObj.getProperty(ROTATE_VELOSITY_KEY)) {
      throw errorMessageNoProperty(ROTATE_VELOSITY_KEY);
    }
    return this.gameObj.getProperty(ROTATE_VELOSITY_KEY);
  }
}
